package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import controller.*;
import model.*;

public class Zeichenflaeche extends JPanel {

	private BunteRechteckeController controller2;

	public Zeichenflaeche(final BunteRechteckeController controller2) {
		this.controller2 = controller2;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for (Rechteck rechteck : controller2.getRechtecke()) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}

	}

}
