package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.RechteckGUI;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		//this.rechtecke = new LinkedList<Rechteck>();
		this.database  = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}
	
	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void reset() {
		rechtecke.clear();
	}

	public void add(Rechteck rechteck) {
		this.database.rechteckEintragen(rechteck);
		rechtecke.add(rechteck);
		
	}

	

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		
		reset();
		
		for (int i = 0; i < 25; i++) {
			
			rechtecke.add(Rechteck.generiereZufallsRechteck());
			
		}
	}

	public static void main(String[] args) {

	}

	public void rechteckHinzufuegen() {
		
		new RechteckGUI(this);
		
	}

}
