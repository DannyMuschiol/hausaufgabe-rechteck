package test;

import model.*;
import controller.*;

public class RechteckTest {

	public static void main(String[] args) {

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		if (rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]")) {
			System.out.println(rechteck0.toString());
		}

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);

		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);

		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);

		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);

		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);

		Rechteck rechteck10 = new Rechteck();
		rechteck10 = Rechteck.generiereZufallsRechteck();

		BunteRechteckeController controller1 = new BunteRechteckeController();
		controller1.add(rechteck0);
		controller1.add(rechteck1);
		controller1.add(rechteck2);
		controller1.add(rechteck3);
		controller1.add(rechteck4);
		controller1.add(rechteck5);
		controller1.add(rechteck6);
		controller1.add(rechteck7);
		controller1.add(rechteck8);
		controller1.add(rechteck9);
		controller1.add(rechteck10);

		System.out.println(controller1.toString());

		// Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10); // Rechteck [x=-4, y=-5, breite=50,hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);// Rechteck [x=-10, y=-10, breite=200,hoehe=100]

		if (rechteckeTesten() == true) {
			System.out.println("Alle Rechtecke befinden sich im vorgegebenen Bereich :)");
		} else {
			System.out.println("Mindestens ein Rechteck befindet sich nicht im vorgegebenen Bereich -.-");
		}

	}

	public static boolean rechteckeTesten() {
		Rechteck[] rechtecke = new Rechteck[50000];

		Rechteck rechteckGrenze = new Rechteck(0, 0, 1200, 1000);

		for (int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
		}

		for (int i = 0; i < rechtecke.length; i++) {

			if (rechteckGrenze.enthaelt(rechtecke[i])) {

				return true;

			}

		}

		return false;

	}

}
