package model;


public class Rechteck {

	private int x;
	private int y;
	private int breite;
	private int hoehe;
	
	public Rechteck() {
		x=0;
		y=0;
		breite = 0;
		hoehe = 0;
		
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		this.x=x;
		this.y=y;
		this.breite = Math.abs(breite);
		this.hoehe = Math.abs(hoehe);
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getBreite() {
		
		return breite;
	}
	
	public void setBreite(int breite) {
		
		if (breite < 0) {
			breite = breite * (-1);
		}
		
		this.breite = breite;
	}
	
	public int getHoehe() {
		
		return hoehe;
	}
	
	public void setHoehe(int hoehe) {
		
		if (hoehe < 0) {
			hoehe = hoehe * (-1);
		}
		
		
		this.hoehe = hoehe;
	}
	
	public boolean enthaelt(int x, int y) {
		if (this.getX() <= x && this.getX() + this.getBreite() >= x && this.getY() <= y && this.getY() + this.getHoehe() >= y) {
			return true;
		}
		return false;
		
	}
	
	public boolean enthaelt(Punkt p) {
		
		return enthaelt(p.getX(), p.getY());
		
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		
		return enthaelt(rechteck.getX(), rechteck.getY()) && enthaelt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Rechteck rechteck = new Rechteck();
		rechteck.setX((int) (Math.random()*1200));
		rechteck.setY((int) (Math.random()*1000));
		rechteck.setBreite((int) (Math.random()*(1200-rechteck.getX())));
		rechteck.setHoehe((int) (Math.random()*(1000-rechteck.getX())));
		return rechteck;
	}
	

	
}
