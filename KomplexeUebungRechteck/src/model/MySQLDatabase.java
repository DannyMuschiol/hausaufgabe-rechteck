package model;

import java.util.LinkedList;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class MySQLDatabase {
	
	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";
	
	public void rechteckEintragen(Rechteck r) {
		
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Rechtecke (x, y, breite, hoehe) VALUES (?, ?, ?, ?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			con.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public List<Rechteck> getAlleRechtecke() {
		List<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Rechtecke";
			Statement s = con.createStatement();
			s.executeQuery(sql);
			ResultSet rs = s.executeQuery(sql);

			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x, y, breite, hoehe));
			}
			
			con.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}
